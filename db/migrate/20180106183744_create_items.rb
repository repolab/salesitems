class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :product
      t.string :seller
      t.date :date_posted
      t.decimal :amount

      t.timestamps null: false
    end
  end
end
