class ItemsController < ApplicationController

    def index
        @items = Item.all
        User.all
    end

    def create
        @item = Item.new(user_params)
        if @item.save
            redirect_to "/items", notice: "Item was successfully created."
        else
            flash[:errors_register] = @item.errors.full_messages
            redirect_to :back
        end
    end
  
    private
        def user_params
            params.require(:item).permit(:product, :amount)
        end

    def destroy
        redirect_to :back
    end

end