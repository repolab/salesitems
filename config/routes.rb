Rails.application.routes.draw do
    root 'users#index'
    post '/users' => 'users#create'
    get '/items' => 'items#index'
    get '/purchases' => 'purchases#index'
    post '/items' => 'items#create'
    post '/sessions' => 'items#index'
    get '/sessions' => 'items#index'
    delete '/sessions' => 'sessions#destroy'
    get '/users' => 'users#index'
    get '/purchases/add' => 'purchases#index'
    get '/destroy' => 'items#destroy'
end
